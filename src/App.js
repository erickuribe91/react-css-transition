import React, { Component } from 'react';
import './App.css';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

function HelloUser(props) {
  return <h1>Hello {props.name}</h1>
}

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userId: 1,
      component: <HelloUser name="User 1" />
    }
  }

  changeUser () {
    let { userId } = this.state
    userId++
    let name = `User ${userId}`
    this.setState({
      userId,
      component: <HelloUser key={name} name={name} />
    })
  }
  render() {
    let { component } = this.state
    return (
      <div className="App">
      <button onClick={this.changeUser.bind(this)}> Next User </button>
      <ReactCSSTransitionGroup
        transitionName="users-transitions"
        transitionEnterTimeout={300}
        transitionLeaveTimeout={500}
        transitionAppear={true}
        transitionAppearTimeout={500}
        >
        {component}
      </ReactCSSTransitionGroup>
      <style jxs>{`
      .users-transitions-appear {
        opacity: 0.01;
      }
      
      .users-transitions-appear.questions-appear-active {
        opacity: 1;
        transition: opacity 5000ms ease-in;
      }
      .users-transitions-enter {
        opacity: 0.01;
        transform: translateX(-100vw)
      }
      
      .users-transitions-enter.users-transitions-enter-active {
        opacity: 1;
        transition: 1000ms ease-out;
        transform: translateX(100%)
      }
      
      .users-transitions-leave {
        opacity: 1;
        transform: translateX(0%)
      }
      
      .users-transitions-leave.users-transitions-leave-active {
        opacity: 0.01;
        transition: 1000ms ease-out;
        font-size: 2px;
        transform: translateX(100vw)

      }
      `}</style>
    </div>
    )
  }
}

export default App
