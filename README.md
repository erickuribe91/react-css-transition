# React CSS Transitions

## Available Scripts

The fisrt step is install the dependences:

#### `npm i`

For run the project, you need run:

##### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3006](http://localhost:3006) to view it in the browser.

## ReactCSSTransitionGroup

You can see more about this in [ReactCSSTransitionGroup](https://reactjs.org/docs/animation.html)


## Customize the transitions

For customize the transition that you need, first is to define the name for the transitions:

`<ReactCSSTransitionGroup transitionName="users-transitions" />`

In this case our name will be `users-transitions`

With this we can define all transition for the elements or component that are inside of different  `ReactCSSTransitionGroup` 


### Apear
This event happens when the component with the `ReactCSSTransitionGroup` is initialized

For customize you need add the css:


	.users-transitions-appear {
      // add your css	
    }

	.users-transitions-appear.questions-appear-active {
	  // add your css
    }

the first element css allows customize la transition when is initialized the event and the second element is how end  the transition

You can add anything transition that you know

### Enter
This event happens when the component detect a change in the state inside the `ReactCSSTransitionGroup` (in the preview state)

For customize you need add the css:

	.users-transitions-enter { 
      // add your css
    }

	.users-transitions-enter.users-transitions-enter-active { 
	  // add your css    
    }


the first element css allows customize la transition when is initialized the event and the second element is how end  the transition

You can add anything transition that you know

### Leave

This event happens when the component detect a change in the state inside the `ReactCSSTransitionGroup`(in the new state)

For customize you need add the css:

	.users-transitions-leave {
	  // add your css
	}
      
    .users-transitions-leave.users-transitions-leave-active {
      // add your css
    }

the first element css allows customize the transition when is initialized the event and the second element is how end  the transition

You can add anything transition that you know

**don't forget that to run the animation you need to change the key of your component when you made a change in the state**

